package com.legacy.premium_wood.block;

import com.legacy.premium_wood.client.container.PremiumWorkbenchContainer;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.CraftingTableBlock;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.inventory.container.SimpleNamedContainerProvider;
import net.minecraft.util.IWorldPosCallable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

public class PremiumWorkbenchBlock extends CraftingTableBlock
{
	private static final ITextComponent GUI_TITLE = new TranslationTextComponent("container.crafting");

	public PremiumWorkbenchBlock()
	{
		super(Block.Properties.from(Blocks.CRAFTING_TABLE));
	}

	@Override
	public INamedContainerProvider getContainer(BlockState state, World worldIn, BlockPos pos)
	{
		return new SimpleNamedContainerProvider((id, inventory, entity) -> { return new PremiumWorkbenchContainer(id, inventory, IWorldPosCallable.of(worldIn, pos), this); }, GUI_TITLE);
	}
}