package com.legacy.premium_wood.block.natural;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.LeavesBlock;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

public class AppleLeavesBlock extends LeavesBlock
{
	public static final BooleanProperty MATURE = BooleanProperty.create("mature");
	public static final BooleanProperty CAN_MATURE = BooleanProperty.create("can_mature");

	public AppleLeavesBlock(Properties properties)
	{
		super(properties);
		this.setDefaultState(super.getDefaultState().with(MATURE, Boolean.valueOf(false)).with(CAN_MATURE, Boolean.valueOf(false)));
	}

	@Override
	protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder)
	{
		super.fillStateContainer(builder);
		builder.add(MATURE).add(CAN_MATURE);
	}

	@Override
	public boolean ticksRandomly(BlockState state)
	{
		return super.ticksRandomly(state) || state.get(CAN_MATURE) && state.get(MATURE).booleanValue() == false;
	}

	@Override
	public void randomTick(BlockState state, ServerWorld worldIn, BlockPos pos, Random random)
	{
		super.randomTick(state, worldIn, pos, random);

		if (state.get(CAN_MATURE).booleanValue() == true && state.get(MATURE).booleanValue() == false && random.nextInt(60) == 0 && !worldIn.isRemote)
		{
			worldIn.setBlockState(pos, state.with(MATURE, Boolean.valueOf(true)));
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit)
	{
		if (state.get(MATURE))
		{
			BlockPos offsetPos = pos.offset(hit.getFace());
			worldIn.addEntity(new ItemEntity(worldIn, offsetPos.getX() + 0.5F, offsetPos.getY() + 0.5F, offsetPos.getZ() + 0.5F, new ItemStack(Items.APPLE)));
			worldIn.playSound((PlayerEntity) null, pos, SoundEvents.BLOCK_GRASS_HIT, SoundCategory.BLOCKS, 1.0F, 1.0F);
			worldIn.playSound((PlayerEntity) null, pos, SoundEvents.ENTITY_ITEM_PICKUP, SoundCategory.BLOCKS, 0.5F, 0.5F);
			worldIn.setBlockState(pos, state.with(MATURE, Boolean.valueOf(false)));
			return ActionResultType.SUCCESS;
		}

		return super.onBlockActivated(state, worldIn, pos, player, handIn, hit);
	}
}
