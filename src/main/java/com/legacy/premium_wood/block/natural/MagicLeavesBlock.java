package com.legacy.premium_wood.block.natural;

import java.util.Random;

import net.minecraft.block.BlockState;
import net.minecraft.block.LeavesBlock;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class MagicLeavesBlock extends LeavesBlock
{
	public MagicLeavesBlock(Properties properties)
	{
		super(properties);
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public void animateTick(BlockState stateIn, World worldIn, BlockPos pos, Random rand)
	{
		super.animateTick(stateIn, worldIn, pos, rand);

		float width = 1.2F;
		for (int i = 0; i < 5; ++i)
		{
			worldIn.addParticle(ParticleTypes.RAIN, pos.getX() - 0.1F + rand.nextDouble() * width, pos.getY() - 0.1F + rand.nextDouble() * width, pos.getZ() - 0.1F + rand.nextDouble() * width, 0.0D, 0.0D, 0.0D);
		}
	}
}
