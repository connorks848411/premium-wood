package com.legacy.premium_wood.block.natural;

import java.util.Random;

import net.minecraft.block.BlockState;
import net.minecraft.block.SaplingBlock;
import net.minecraft.block.trees.Tree;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.Explosion;
import net.minecraft.world.server.ServerWorld;

public class ExplodingSaplingBlock extends SaplingBlock
{
	public ExplodingSaplingBlock(Tree treeIn, Properties properties)
	{
		super(treeIn, properties);
	}

	@Override
	public void grow(ServerWorld worldIn, Random rand, BlockPos pos, BlockState state)
	{
		if (rand.nextInt(3) != 1)
		{
			worldIn.removeBlock(pos, false);
			worldIn.getWorld().createExplosion(null, pos.getX(), pos.getY(), pos.getZ(), 4, Explosion.Mode.DESTROY);
		}
		else
		{
			super.grow(worldIn, rand, pos, state);
		}
	}
}
