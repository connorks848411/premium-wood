package com.legacy.premium_wood.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorldReader;
import net.minecraftforge.common.extensions.IForgeBlock;

public class PremiumBookshelfBlock extends Block implements IForgeBlock
{
	public PremiumBookshelfBlock()
	{
		super(Block.Properties.from(Blocks.BOOKSHELF));
	}

	@Override
	public float getEnchantPowerBonus(BlockState state, IWorldReader world, BlockPos pos)
	{
		return 1.0F;
	}
}
