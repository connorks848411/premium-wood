package com.legacy.premium_wood.block;

import java.util.LinkedHashMap;
import java.util.Map;

import com.legacy.premium_wood.PremiumRegistry;
import com.legacy.premium_wood.block.natural.AppleLeavesBlock;
import com.legacy.premium_wood.block.natural.ExplodingSaplingBlock;
import com.legacy.premium_wood.block.natural.MagicLeavesBlock;
import com.legacy.premium_wood.item.tree.AppleTree;
import com.legacy.premium_wood.item.tree.MagicTree;
import com.legacy.premium_wood.item.tree.MapleTree;
import com.legacy.premium_wood.item.tree.PurpleHeartTree;
import com.legacy.premium_wood.item.tree.SilverbellTree;
import com.legacy.premium_wood.item.tree.TigerTree;
import com.legacy.premium_wood.item.tree.WillowTree;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.DoorBlock;
import net.minecraft.block.FenceBlock;
import net.minecraft.block.FenceGateBlock;
import net.minecraft.block.GlassBlock;
import net.minecraft.block.LeavesBlock;
import net.minecraft.block.LogBlock;
import net.minecraft.block.PressurePlateBlock;
import net.minecraft.block.SaplingBlock;
import net.minecraft.block.SlabBlock;
import net.minecraft.block.StairsBlock;
import net.minecraft.block.TrapDoorBlock;
import net.minecraft.block.WoodButtonBlock;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.registries.IForgeRegistry;

public class PremiumBlocks
{
	public static Block maple_crafting_table, maple_leaves, maple_log, maple_planks, maple_stairs, maple_slab, maple_fence, maple_fence_gate, maple_door, maple_trapdoor, maple_pressure_plate, maple_button, maple_sapling, maple_bookshelf, maple_framed_glass;

	public static Block tiger_crafting_table, tiger_leaves, tiger_log, tiger_planks, tiger_stairs, tiger_slab, tiger_fence, tiger_fence_gate, tiger_door, tiger_trapdoor, tiger_pressure_plate, tiger_button, tiger_sapling, tiger_bookshelf, tiger_framed_glass;

	public static Block silverbell_crafting_table, silverbell_leaves, silverbell_log, silverbell_planks, silverbell_stairs, silverbell_slab, silverbell_fence, silverbell_fence_gate, silverbell_door, silverbell_trapdoor, silverbell_pressure_plate, silverbell_button, silverbell_sapling, silverbell_bookshelf, silverbell_framed_glass;

	public static Block purple_heart_crafting_table, purple_heart_leaves, purple_heart_log, purple_heart_planks, purple_heart_stairs, purple_heart_slab, purple_heart_fence, purple_heart_fence_gate, purple_heart_door, purple_heart_trapdoor, purple_heart_pressure_plate, purple_heart_button, purple_heart_sapling, purple_heart_bookshelf, purple_heart_framed_glass;

	public static Block willow_crafting_table, willow_leaves, willow_log, willow_planks, willow_stairs, willow_slab, willow_fence, willow_fence_gate, willow_door, willow_trapdoor, willow_pressure_plate, willow_button, willow_sapling, willow_bookshelf, willow_framed_glass;

	public static Block magic_crafting_table, magic_leaves, magic_log, magic_planks, magic_stairs, magic_slab, magic_fence, magic_fence_gate, magic_door, magic_trapdoor, magic_pressure_plate, magic_button, magic_sapling, magic_bookshelf, magic_framed_glass;

	public static Block apple_leaves, apple_sapling;

	public static Block potted_maple_sapling, potted_tiger_sapling, potted_silverbell_sapling, potted_purple_heart_sapling, potted_willow_sapling, potted_magic_sapling, potted_apple_sapling;

	public static Map<Block, ItemGroup> blockItemMap = new LinkedHashMap<>();
	public static Map<Block, Item.Properties> blockItemPropertiesMap = new LinkedHashMap<>();

	private static IForgeRegistry<Block> iBlockRegistry;

	public static void init(Register<Block> event)
	{
		PremiumBlocks.iBlockRegistry = event.getRegistry();
		ItemGroup decorTab = ItemGroup.DECORATIONS;

		maple_crafting_table = register("maple_crafting_table", new PremiumWorkbenchBlock() {});
		maple_leaves = register("maple_leaves", new LeavesBlock(Block.Properties.from(Blocks.OAK_LEAVES)));
		maple_log = register("maple_log", new LogBlock(MaterialColor.PINK_TERRACOTTA, Block.Properties.from(Blocks.OAK_LOG)));
		maple_planks = register("maple_planks", new Block(Block.Properties.from(Blocks.OAK_PLANKS)));
		maple_bookshelf = register("maple_bookshelf", new PremiumBookshelfBlock());
		maple_stairs = register("maple_stairs", new StairsBlock(() -> PremiumBlocks.maple_planks.getDefaultState(), Block.Properties.from(Blocks.OAK_STAIRS)));
		maple_slab = register("maple_slab", new SlabBlock(Block.Properties.from(Blocks.OAK_SLAB)) {});
		maple_fence = register("maple_fence", new FenceBlock(Block.Properties.from(Blocks.OAK_FENCE)), decorTab);
		maple_fence_gate = register("maple_fence_gate", new FenceGateBlock(Block.Properties.from(Blocks.OAK_FENCE_GATE)), ItemGroup.REDSTONE);
		maple_door = register("maple_door", new DoorBlock(Block.Properties.from(Blocks.OAK_DOOR)) {}, ItemGroup.REDSTONE);
		maple_trapdoor = register("maple_trapdoor", new TrapDoorBlock(Block.Properties.from(Blocks.OAK_TRAPDOOR)) {}, ItemGroup.REDSTONE);
		maple_pressure_plate = register("maple_pressure_plate", new PressurePlateBlock(PressurePlateBlock.Sensitivity.EVERYTHING, Block.Properties.from(Blocks.OAK_PRESSURE_PLATE)) {}, ItemGroup.REDSTONE);
		maple_button = register("maple_button", new WoodButtonBlock(Block.Properties.from(Blocks.OAK_BUTTON)) {}, ItemGroup.REDSTONE);
		maple_sapling = register("maple_sapling", new SaplingBlock(new MapleTree(), Block.Properties.from(Blocks.OAK_SAPLING)) {}, decorTab);
		maple_framed_glass = register("maple_framed_glass", new GlassBlock(Block.Properties.from(Blocks.GLASS)));

		tiger_crafting_table = register("tiger_crafting_table", new PremiumWorkbenchBlock() {});
		tiger_leaves = register("tiger_leaves", new LeavesBlock(Block.Properties.from(Blocks.OAK_LEAVES)));
		tiger_log = register("tiger_log", new LogBlock(MaterialColor.PINK_TERRACOTTA, Block.Properties.from(Blocks.OAK_LOG)));
		tiger_planks = register("tiger_planks", new Block(Block.Properties.from(Blocks.OAK_PLANKS)));
		tiger_bookshelf = register("tiger_bookshelf", new PremiumBookshelfBlock());
		tiger_stairs = register("tiger_stairs", new StairsBlock(() -> PremiumBlocks.tiger_planks.getDefaultState(), Block.Properties.from(Blocks.OAK_STAIRS)));
		tiger_slab = register("tiger_slab", new SlabBlock(Block.Properties.from(Blocks.OAK_SLAB)) {});
		tiger_fence = register("tiger_fence", new FenceBlock(Block.Properties.from(Blocks.OAK_FENCE)), decorTab);
		tiger_fence_gate = register("tiger_fence_gate", new FenceGateBlock(Block.Properties.from(Blocks.OAK_FENCE_GATE)), ItemGroup.REDSTONE);
		tiger_door = register("tiger_door", new DoorBlock(Block.Properties.from(Blocks.OAK_DOOR)) {}, ItemGroup.REDSTONE);
		tiger_trapdoor = register("tiger_trapdoor", new TrapDoorBlock(Block.Properties.from(Blocks.OAK_TRAPDOOR)) {}, ItemGroup.REDSTONE);
		tiger_pressure_plate = register("tiger_pressure_plate", new PressurePlateBlock(PressurePlateBlock.Sensitivity.EVERYTHING, Block.Properties.from(Blocks.OAK_PRESSURE_PLATE)) {}, ItemGroup.REDSTONE);
		tiger_button = register("tiger_button", new WoodButtonBlock(Block.Properties.from(Blocks.OAK_BUTTON)) {}, ItemGroup.REDSTONE);
		tiger_sapling = register("tiger_sapling", new SaplingBlock(new TigerTree(), Block.Properties.from(Blocks.OAK_SAPLING)) {}, decorTab);
		tiger_framed_glass = register("tiger_framed_glass", new GlassBlock(Block.Properties.from(Blocks.GLASS)));

		silverbell_crafting_table = register("silverbell_crafting_table", new PremiumWorkbenchBlock() {});
		silverbell_leaves = register("silverbell_leaves", new LeavesBlock(Block.Properties.from(Blocks.OAK_LEAVES)));
		silverbell_log = register("silverbell_log", new LogBlock(MaterialColor.PINK_TERRACOTTA, Block.Properties.from(Blocks.OAK_LOG)));
		silverbell_planks = register("silverbell_planks", new Block(Block.Properties.from(Blocks.OAK_PLANKS)));
		silverbell_bookshelf = register("silverbell_bookshelf", new PremiumBookshelfBlock());
		silverbell_stairs = register("silverbell_stairs", new StairsBlock(() -> PremiumBlocks.silverbell_planks.getDefaultState(), Block.Properties.from(Blocks.OAK_STAIRS)));
		silverbell_slab = register("silverbell_slab", new SlabBlock(Block.Properties.from(Blocks.OAK_SLAB)) {});
		silverbell_fence = register("silverbell_fence", new FenceBlock(Block.Properties.from(Blocks.OAK_FENCE)), decorTab);
		silverbell_fence_gate = register("silverbell_fence_gate", new FenceGateBlock(Block.Properties.from(Blocks.OAK_FENCE_GATE)), ItemGroup.REDSTONE);
		silverbell_door = register("silverbell_door", new DoorBlock(Block.Properties.from(Blocks.OAK_DOOR)) {}, ItemGroup.REDSTONE);
		silverbell_trapdoor = register("silverbell_trapdoor", new TrapDoorBlock(Block.Properties.from(Blocks.OAK_TRAPDOOR)) {}, ItemGroup.REDSTONE);
		silverbell_pressure_plate = register("silverbell_pressure_plate", new PressurePlateBlock(PressurePlateBlock.Sensitivity.EVERYTHING, Block.Properties.from(Blocks.OAK_PRESSURE_PLATE)) {}, ItemGroup.REDSTONE);
		silverbell_button = register("silverbell_button", new WoodButtonBlock(Block.Properties.from(Blocks.OAK_BUTTON)) {}, ItemGroup.REDSTONE);
		silverbell_sapling = register("silverbell_sapling", new SaplingBlock(new SilverbellTree(), Block.Properties.from(Blocks.OAK_SAPLING)) {}, decorTab);
		silverbell_framed_glass = register("silverbell_framed_glass", new GlassBlock(Block.Properties.from(Blocks.GLASS)));

		purple_heart_crafting_table = register("purple_heart_crafting_table", new PremiumWorkbenchBlock() {});
		purple_heart_leaves = register("purple_heart_leaves", new LeavesBlock(Block.Properties.from(Blocks.OAK_LEAVES)));
		purple_heart_log = register("purple_heart_log", new LogBlock(MaterialColor.PINK_TERRACOTTA, Block.Properties.from(Blocks.OAK_LOG)));
		purple_heart_planks = register("purple_heart_planks", new Block(Block.Properties.from(Blocks.OAK_PLANKS)));
		purple_heart_bookshelf = register("purple_heart_bookshelf", new PremiumBookshelfBlock());
		purple_heart_stairs = register("purple_heart_stairs", new StairsBlock(() -> PremiumBlocks.purple_heart_planks.getDefaultState(), Block.Properties.from(Blocks.OAK_STAIRS)));
		purple_heart_slab = register("purple_heart_slab", new SlabBlock(Block.Properties.from(Blocks.OAK_SLAB)) {});
		purple_heart_fence = register("purple_heart_fence", new FenceBlock(Block.Properties.from(Blocks.OAK_FENCE)), decorTab);
		purple_heart_fence_gate = register("purple_heart_fence_gate", new FenceGateBlock(Block.Properties.from(Blocks.OAK_FENCE_GATE)), ItemGroup.REDSTONE);
		purple_heart_door = register("purple_heart_door", new DoorBlock(Block.Properties.from(Blocks.OAK_DOOR)) {}, ItemGroup.REDSTONE);
		purple_heart_trapdoor = register("purple_heart_trapdoor", new TrapDoorBlock(Block.Properties.from(Blocks.OAK_TRAPDOOR)) {}, ItemGroup.REDSTONE);
		purple_heart_pressure_plate = register("purple_heart_pressure_plate", new PressurePlateBlock(PressurePlateBlock.Sensitivity.EVERYTHING, Block.Properties.from(Blocks.OAK_PRESSURE_PLATE)) {}, ItemGroup.REDSTONE);
		purple_heart_button = register("purple_heart_button", new WoodButtonBlock(Block.Properties.from(Blocks.OAK_BUTTON)) {}, ItemGroup.REDSTONE);
		purple_heart_sapling = register("purple_heart_sapling", new SaplingBlock(new PurpleHeartTree(), Block.Properties.from(Blocks.OAK_SAPLING)) {}, decorTab);
		purple_heart_framed_glass = register("purple_heart_framed_glass", new GlassBlock(Block.Properties.from(Blocks.GLASS)));

		willow_crafting_table = register("willow_crafting_table", new PremiumWorkbenchBlock() {});
		willow_leaves = register("willow_leaves", new LeavesBlock(Block.Properties.from(Blocks.OAK_LEAVES)));
		willow_log = register("willow_log", new LogBlock(MaterialColor.GREEN, Block.Properties.from(Blocks.OAK_LOG)));
		willow_planks = register("willow_planks", new Block(Block.Properties.from(Blocks.OAK_PLANKS)));
		willow_bookshelf = register("willow_bookshelf", new PremiumBookshelfBlock());
		willow_stairs = register("willow_stairs", new StairsBlock(() -> PremiumBlocks.willow_planks.getDefaultState(), Block.Properties.from(Blocks.OAK_STAIRS)));
		willow_slab = register("willow_slab", new SlabBlock(Block.Properties.from(Blocks.OAK_SLAB)) {});
		willow_fence = register("willow_fence", new FenceBlock(Block.Properties.from(Blocks.OAK_FENCE)), decorTab);
		willow_fence_gate = register("willow_fence_gate", new FenceGateBlock(Block.Properties.from(Blocks.OAK_FENCE_GATE)), ItemGroup.REDSTONE);
		willow_door = register("willow_door", new DoorBlock(Block.Properties.from(Blocks.OAK_DOOR)) {}, ItemGroup.REDSTONE);
		willow_trapdoor = register("willow_trapdoor", new TrapDoorBlock(Block.Properties.from(Blocks.OAK_TRAPDOOR)) {}, ItemGroup.REDSTONE);
		willow_pressure_plate = register("willow_pressure_plate", new PressurePlateBlock(PressurePlateBlock.Sensitivity.EVERYTHING, Block.Properties.from(Blocks.OAK_PRESSURE_PLATE)) {}, ItemGroup.REDSTONE);
		willow_button = register("willow_button", new WoodButtonBlock(Block.Properties.from(Blocks.OAK_BUTTON)) {}, ItemGroup.REDSTONE);
		willow_sapling = register("willow_sapling", new SaplingBlock(new WillowTree(), Block.Properties.from(Blocks.OAK_SAPLING)) {}, decorTab);
		willow_framed_glass = register("willow_framed_glass", new GlassBlock(Block.Properties.from(Blocks.GLASS)));

		magic_crafting_table = register("magic_crafting_table", new PremiumWorkbenchBlock() {});
		magic_leaves = register("magic_leaves", new MagicLeavesBlock(Block.Properties.from(Blocks.OAK_LEAVES)));
		magic_log = register("magic_log", new LogBlock(MaterialColor.PINK_TERRACOTTA, Block.Properties.from(Blocks.OAK_LOG)));
		magic_planks = register("magic_planks", new Block(Block.Properties.from(Blocks.OAK_PLANKS)));
		magic_bookshelf = register("magic_bookshelf", new PremiumBookshelfBlock());
		magic_stairs = register("magic_stairs", new StairsBlock(() -> PremiumBlocks.magic_planks.getDefaultState(), Block.Properties.from(Blocks.OAK_STAIRS)));
		magic_slab = register("magic_slab", new SlabBlock(Block.Properties.from(Blocks.OAK_SLAB)) {});
		magic_fence = register("magic_fence", new FenceBlock(Block.Properties.from(Blocks.OAK_FENCE)), decorTab);
		magic_fence_gate = register("magic_fence_gate", new FenceGateBlock(Block.Properties.from(Blocks.OAK_FENCE_GATE)), ItemGroup.REDSTONE);
		magic_door = register("magic_door", new DoorBlock(Block.Properties.from(Blocks.OAK_DOOR)) {}, ItemGroup.REDSTONE);
		magic_trapdoor = register("magic_trapdoor", new TrapDoorBlock(Block.Properties.from(Blocks.OAK_TRAPDOOR)) {}, ItemGroup.REDSTONE);
		magic_pressure_plate = register("magic_pressure_plate", new PressurePlateBlock(PressurePlateBlock.Sensitivity.EVERYTHING, Block.Properties.from(Blocks.OAK_PRESSURE_PLATE)) {}, ItemGroup.REDSTONE);
		magic_button = register("magic_button", new WoodButtonBlock(Block.Properties.from(Blocks.OAK_BUTTON)) {}, ItemGroup.REDSTONE);
		magic_sapling = register("magic_sapling", new ExplodingSaplingBlock(new MagicTree(), Block.Properties.from(Blocks.OAK_SAPLING)), decorTab);
		magic_framed_glass = register("magic_framed_glass", new GlassBlock(Block.Properties.from(Blocks.GLASS)));

		apple_leaves = register("apple_leaves", new AppleLeavesBlock(Block.Properties.from(Blocks.OAK_LEAVES)));
		apple_sapling = register("apple_sapling", new SaplingBlock(new AppleTree(), Block.Properties.from(Blocks.OAK_SAPLING)) {}, decorTab);
		
		potted_maple_sapling = registerBlock("potted_maple_sapling", new PremiumFlowerPotBlock(() -> maple_sapling.delegate.get()));
		potted_tiger_sapling = registerBlock("potted_tiger_sapling", new PremiumFlowerPotBlock(() -> tiger_sapling.delegate.get()));
		potted_silverbell_sapling = registerBlock("potted_silverbell_sapling", new PremiumFlowerPotBlock(() -> silverbell_sapling.delegate.get()));
		potted_purple_heart_sapling = registerBlock("potted_purple_heart_sapling", new PremiumFlowerPotBlock(() -> purple_heart_sapling.delegate.get()));
		potted_willow_sapling = registerBlock("potted_willow_sapling", new PremiumFlowerPotBlock(() -> willow_sapling.delegate.get()));
		potted_magic_sapling = registerBlock("potted_magic_sapling", new PremiumFlowerPotBlock(() -> magic_sapling.delegate.get()));
		potted_apple_sapling = registerBlock("potted_apple_sapling", new PremiumFlowerPotBlock(() -> apple_sapling.delegate.get()));
	}

	public static void setBlockRegistry(IForgeRegistry<Block> iBlockRegistry)
	{
		PremiumBlocks.iBlockRegistry = iBlockRegistry;
	}

	public static Block register(String name, Block block)
	{
		register(name, block, ItemGroup.BUILDING_BLOCKS);
		return block;
	}

	public static <T extends ItemGroup> Block register(String key, Block block, T itemGroup)
	{
		blockItemMap.put(block, itemGroup);
		return registerBlock(key, block);
	}

	public static Block registerBlock(String name, Block block)
	{
		if (iBlockRegistry != null)
		{
			PremiumRegistry.register(iBlockRegistry, name, block);
		}

		return block;
	}
}