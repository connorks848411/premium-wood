package com.legacy.premium_wood;

import com.legacy.premium_wood.block.PremiumBlocks;
import com.legacy.premium_wood.world.AppleTreeFeature;
import com.legacy.premium_wood.world.PremiumTreeFeature;
import com.legacy.premium_wood.world.WillowTreeFeature;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biome.Category;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.biome.DefaultBiomeFeatures;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.TreeFeatureConfig;
import net.minecraft.world.gen.placement.AtSurfaceWithExtraConfig;
import net.minecraft.world.gen.placement.Placement;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.ForgeRegistries;

@Mod(PremiumWoodMod.MODID)
public class PremiumWoodMod
{
	public static final String NAME = "Premium Wood";

	public static final String MODID = "premium_wood";

	public static ResourceLocation locate(String name)
	{
		return new ResourceLocation(PremiumWoodMod.MODID, name);
	}

	public static String find(String name)
	{
		return PremiumWoodMod.MODID + ":" + name;
	}

	public PremiumWoodMod()
	{
		FMLJavaModLoadingContext.get().getModEventBus().addListener(PremiumWoodMod::commonInit);
		DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> FMLJavaModLoadingContext.get().getModEventBus().addListener(PremiumWoodMod::clientInit));
	}

	public static void commonInit(FMLCommonSetupEvent event)
	{
		for (Biome biomeIn : ForgeRegistries.BIOMES.getValues())
		{
			if (biomeIn == Biomes.PLAINS)
			{
				biomeIn.addFeature(GenerationStage.Decoration.VEGETAL_DECORATION, new AppleTreeFeature(TreeFeatureConfig::func_227338_a_, false).withConfiguration(DefaultBiomeFeatures.OAK_TREE_CONFIG).withPlacement(Placement.COUNT_EXTRA_HEIGHTMAP.configure(new AtSurfaceWithExtraConfig(0, 0.1F, 1))));
			}

			if (biomeIn == Biomes.FOREST || biomeIn == Biomes.FLOWER_FOREST || biomeIn == Biomes.WOODED_HILLS)
			{
				biomeIn.addFeature(GenerationStage.Decoration.VEGETAL_DECORATION, new PremiumTreeFeature(TreeFeatureConfig::func_227338_a_, false, 4, PremiumBlocks.maple_log.getDefaultState(), PremiumBlocks.maple_leaves.getDefaultState()).withConfiguration(DefaultBiomeFeatures.OAK_TREE_CONFIG).withPlacement(Placement.COUNT_EXTRA_HEIGHTMAP.configure(new AtSurfaceWithExtraConfig(0, 0.3F, 1))));
			}

			if (biomeIn == Biomes.BIRCH_FOREST || biomeIn == Biomes.BIRCH_FOREST_HILLS || biomeIn == Biomes.TALL_BIRCH_FOREST || biomeIn == Biomes.TALL_BIRCH_HILLS)
			{
				biomeIn.addFeature(GenerationStage.Decoration.VEGETAL_DECORATION, new PremiumTreeFeature(TreeFeatureConfig::func_227338_a_, false, 4, PremiumBlocks.silverbell_log.getDefaultState(), PremiumBlocks.silverbell_leaves.getDefaultState()).withConfiguration(DefaultBiomeFeatures.OAK_TREE_CONFIG).withPlacement(Placement.COUNT_EXTRA_HEIGHTMAP.configure(new AtSurfaceWithExtraConfig(0, 0.5F, 1))));
			}

			if (biomeIn == Biomes.SAVANNA || biomeIn == Biomes.SAVANNA_PLATEAU)
			{
				biomeIn.addFeature(GenerationStage.Decoration.VEGETAL_DECORATION, new PremiumTreeFeature(TreeFeatureConfig::func_227338_a_, false, 6, PremiumBlocks.tiger_log.getDefaultState(), PremiumBlocks.tiger_leaves.getDefaultState()).withConfiguration(DefaultBiomeFeatures.OAK_TREE_CONFIG).withPlacement(Placement.COUNT_EXTRA_HEIGHTMAP.configure(new AtSurfaceWithExtraConfig(0, 0.3F, 1))));
			}

			if (biomeIn.getCategory() == Category.JUNGLE)
			{
				biomeIn.addFeature(GenerationStage.Decoration.VEGETAL_DECORATION, new PremiumTreeFeature(TreeFeatureConfig::func_227338_a_, false, 7, PremiumBlocks.purple_heart_log.getDefaultState(), PremiumBlocks.purple_heart_leaves.getDefaultState()).withConfiguration(DefaultBiomeFeatures.OAK_TREE_CONFIG).withPlacement(Placement.COUNT_EXTRA_HEIGHTMAP.configure(new AtSurfaceWithExtraConfig(0, 0.4F, 1))));
			}

			if (biomeIn == Biomes.SWAMP || biomeIn == Biomes.SWAMP_HILLS)
			{
				biomeIn.addFeature(GenerationStage.Decoration.VEGETAL_DECORATION, new WillowTreeFeature(TreeFeatureConfig::func_227338_a_, false).withConfiguration(DefaultBiomeFeatures.OAK_TREE_CONFIG).withPlacement(Placement.COUNT_EXTRA_HEIGHTMAP.configure(new AtSurfaceWithExtraConfig(0, 0.3F, 1))));
			}

			if (biomeIn == Biomes.MOUNTAINS || biomeIn == Biomes.WOODED_MOUNTAINS || biomeIn == Biomes.SNOWY_MOUNTAINS || biomeIn == Biomes.TAIGA || biomeIn == Biomes.TAIGA_HILLS || biomeIn == Biomes.TAIGA_MOUNTAINS || biomeIn == Biomes.SNOWY_TAIGA)
			{
				biomeIn.addFeature(GenerationStage.Decoration.VEGETAL_DECORATION, new PremiumTreeFeature(TreeFeatureConfig::func_227338_a_, false, 4, PremiumBlocks.magic_log.getDefaultState(), PremiumBlocks.magic_leaves.getDefaultState()).withConfiguration(DefaultBiomeFeatures.OAK_TREE_CONFIG).withPlacement(Placement.COUNT_EXTRA_HEIGHTMAP.configure(new AtSurfaceWithExtraConfig(0, 0.1F, 1))));
			}
		}
	}

	public static void clientInit(FMLClientSetupEvent event)
	{
		renderCutout(PremiumBlocks.apple_sapling);
		renderCutout(PremiumBlocks.magic_sapling);
		renderCutout(PremiumBlocks.maple_sapling);
		renderCutout(PremiumBlocks.purple_heart_sapling);
		renderCutout(PremiumBlocks.silverbell_sapling);
		renderCutout(PremiumBlocks.tiger_sapling);
		renderCutout(PremiumBlocks.willow_sapling);

		renderCutout(PremiumBlocks.magic_door);
		renderCutout(PremiumBlocks.maple_door);
		renderCutout(PremiumBlocks.purple_heart_door);
		renderCutout(PremiumBlocks.silverbell_door);
		renderCutout(PremiumBlocks.tiger_door);
		renderCutout(PremiumBlocks.willow_door);

		renderCutout(PremiumBlocks.magic_trapdoor);
		renderCutout(PremiumBlocks.maple_trapdoor);
		renderCutout(PremiumBlocks.purple_heart_trapdoor);
		renderCutout(PremiumBlocks.silverbell_trapdoor);
		renderCutout(PremiumBlocks.tiger_trapdoor);
		renderCutout(PremiumBlocks.willow_trapdoor);

		renderCutout(PremiumBlocks.potted_apple_sapling);
		renderCutout(PremiumBlocks.potted_magic_sapling);
		renderCutout(PremiumBlocks.potted_maple_sapling);
		renderCutout(PremiumBlocks.potted_purple_heart_sapling);
		renderCutout(PremiumBlocks.potted_silverbell_sapling);
		renderCutout(PremiumBlocks.potted_tiger_sapling);
		renderCutout(PremiumBlocks.potted_willow_sapling);

		renderCutout(PremiumBlocks.magic_framed_glass);
		renderCutout(PremiumBlocks.maple_framed_glass);
		renderCutout(PremiumBlocks.purple_heart_framed_glass);
		renderCutout(PremiumBlocks.silverbell_framed_glass);
		renderCutout(PremiumBlocks.tiger_framed_glass);
		renderCutout(PremiumBlocks.willow_framed_glass);
	}

	private static void renderCutout(Block block)
	{
		RenderTypeLookup.setRenderLayer(block, RenderType.getCutout());
	}
}
