package com.legacy.premium_wood.client.container;

import net.minecraft.block.Block;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.WorkbenchContainer;
import net.minecraft.util.IWorldPosCallable;

public class PremiumWorkbenchContainer extends WorkbenchContainer
{
	private final Block workbench;
	private IWorldPosCallable worldPos;

	public PremiumWorkbenchContainer(int id, PlayerInventory playerInv, IWorldPosCallable worldPos, Block workbench)
	{
		super(id, playerInv, worldPos);
		this.workbench = workbench;
		this.worldPos = worldPos;
	}

	@Override
	public boolean canInteractWith(PlayerEntity playerIn)
	{
		return isWithinUsableDistance(this.worldPos, playerIn, this.workbench);
	}

	protected static boolean isWithinUsableDistance(IWorldPosCallable worldPos, PlayerEntity playerIn, Block targetBlock)
	{
		return worldPos.applyOrElse((world, pos) ->
		{
			return world.getBlockState(pos).getBlock() != targetBlock ? false : playerIn.getDistanceSq((double) pos.getX() + 0.5D, (double) pos.getY() + 0.5D, (double) pos.getZ() + 0.5D) <= 64.0D;
		}, true);
	}
}