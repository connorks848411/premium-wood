package com.legacy.premium_wood.item.tree;

import java.util.Random;

import com.legacy.premium_wood.block.PremiumBlocks;
import com.legacy.premium_wood.world.PremiumTreeFeature;

import net.minecraft.block.trees.Tree;
import net.minecraft.world.biome.DefaultBiomeFeatures;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.TreeFeatureConfig;

public class PurpleHeartTree extends Tree
{
	@Override
	protected ConfiguredFeature<TreeFeatureConfig, ?> getTreeFeature(Random p_225546_1_, boolean p_225546_2_)
	{
		return new PremiumTreeFeature(TreeFeatureConfig::func_227338_a_, true, 7, PremiumBlocks.purple_heart_log.getDefaultState(), PremiumBlocks.purple_heart_leaves.getDefaultState()).withConfiguration(DefaultBiomeFeatures.OAK_TREE_CONFIG);
	}
}