package com.legacy.premium_wood.item.tree;

import java.util.Random;

import com.legacy.premium_wood.block.PremiumBlocks;
import com.legacy.premium_wood.world.PremiumTreeFeature;

import net.minecraft.block.trees.Tree;
import net.minecraft.world.biome.DefaultBiomeFeatures;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.TreeFeatureConfig;

public class MapleTree extends Tree
{
	@Override
	protected ConfiguredFeature<TreeFeatureConfig, ?> getTreeFeature(Random p_225546_1_, boolean p_225546_2_)
	{
		return new PremiumTreeFeature(TreeFeatureConfig::func_227338_a_, true, 4, PremiumBlocks.maple_log.getDefaultState(), PremiumBlocks.maple_leaves.getDefaultState()).withConfiguration(DefaultBiomeFeatures.OAK_TREE_CONFIG);
	}
}