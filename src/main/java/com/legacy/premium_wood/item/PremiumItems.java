package com.legacy.premium_wood.item;

import java.util.Map.Entry;

import com.legacy.premium_wood.PremiumRegistry;
import com.legacy.premium_wood.block.PremiumBlocks;

import net.minecraft.block.Block;
import net.minecraft.item.AxeItem;
import net.minecraft.item.BlockItem;
import net.minecraft.item.HoeItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemTier;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.ShovelItem;
import net.minecraft.item.SwordItem;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.registries.IForgeRegistry;

public class PremiumItems
{
	public static Item maple_stick, tiger_stick, silverbell_stick, purple_heart_stick, willow_stick, magic_stick;

	public static Item magic_sword, magic_pickaxe, magic_axe, magic_shovel, magic_hoe, magic_machete;

	private static IForgeRegistry<Item> iItemRegistry;

	public static void init(Register<Item> event)
	{
		PremiumItems.iItemRegistry = event.getRegistry();
		registerBlockItems();

		/*maple_stick = register("maple_stick", new Item(new Item.Properties().group(ItemGroup.MATERIALS)));
		tiger_stick = register("tiger_stick", new Item(new Item.Properties().group(ItemGroup.MATERIALS)));
		silverbell_stick = register("silverbell_stick", new Item(new Item.Properties().group(ItemGroup.MATERIALS)));
		purple_heart_stick = register("purple_heart_stick", new Item(new Item.Properties().group(ItemGroup.MATERIALS)));
		willow_stick = register("willow_stick", new Item(new Item.Properties().group(ItemGroup.MATERIALS)));
		magic_stick = register("magic_stick", new Item(new Item.Properties().group(ItemGroup.MATERIALS)));*/

		magic_sword = register("magic_sword", new SwordItem(ItemTier.WOOD, 3, -2.4F, new Item.Properties().group(ItemGroup.COMBAT)));
		magic_pickaxe = register("magic_pickaxe", new PickaxeItem(ItemTier.WOOD, 1, -2.8F, new Item.Properties().group(ItemGroup.TOOLS)));
		magic_axe = register("magic_axe", new AxeItem(ItemTier.WOOD, 6.0F, -3.2F, new Item.Properties().group(ItemGroup.TOOLS)));
		magic_shovel = register("magic_shovel", new ShovelItem(ItemTier.WOOD, 1.5F, -3.0F, new Item.Properties().group(ItemGroup.TOOLS)));
		magic_hoe = register("magic_hoe", new HoeItem(ItemTier.WOOD, -3.0F, new Item.Properties().group(ItemGroup.TOOLS)));
		//magic_machete = register("magic_machete", new SwordItem(ItemTier.WOOD, 3, -2.4F, new Item.Properties().group(ItemGroup.TOOLS)));
	}

	private static void registerBlockItems()
	{
		for (Entry<Block, ItemGroup> entry : PremiumBlocks.blockItemMap.entrySet())
		{
			PremiumRegistry.register(iItemRegistry, entry.getKey().getRegistryName().getPath(), new BlockItem(entry.getKey(), new Item.Properties().group(entry.getValue())));
		}
		PremiumBlocks.blockItemMap.clear();

		for (Entry<Block, Item.Properties> entry : PremiumBlocks.blockItemPropertiesMap.entrySet())
		{
			PremiumRegistry.register(iItemRegistry, entry.getKey().getRegistryName().getPath(), new BlockItem(entry.getKey(), entry.getValue()));
		}
		PremiumBlocks.blockItemPropertiesMap.clear();
	}

	private static Item register(String name, Item item)
	{
		PremiumRegistry.register(iItemRegistry, name, item);
		return item;
	}
}