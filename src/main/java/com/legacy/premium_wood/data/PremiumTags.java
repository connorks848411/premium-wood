package com.legacy.premium_wood.data;

import com.legacy.premium_wood.PremiumWoodMod;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.Tag;

public class PremiumTags
{
	public static class Blocks
	{
		public static final Tag<Block> MAPLE_LOGS = tag("maple_logs");
		public static final Tag<Block> TIGER_LOGS = tag("tiger_logs");
		public static final Tag<Block> SILVERBELL_LOGS = tag("silverbell_logs");
		public static final Tag<Block> PURPLE_HEART_LOGS = tag("purple_heart_logs");
		public static final Tag<Block> WILLOW_LOGS = tag("willow_logs");
		public static final Tag<Block> MAGIC_LOGS = tag("magic_logs");
		public static final Tag<Block> FRAMED_GLASS = tag("framed_glass");

		private static Tag<Block> tag(String name)
		{
			return new BlockTags.Wrapper(PremiumWoodMod.locate(name));
		}
	}

	public static class Items
	{
		public static final Tag<Item> MAPLE_LOGS = tag("maple_logs");
		public static final Tag<Item> TIGER_LOGS = tag("tiger_logs");
		public static final Tag<Item> SILVERBELL_LOGS = tag("silverbell_logs");
		public static final Tag<Item> PURPLE_HEART_LOGS = tag("purple_heart_logs");
		public static final Tag<Item> WILLOW_LOGS = tag("willow_logs");
		public static final Tag<Item> MAGIC_LOGS = tag("magic_logs");
		public static final Tag<Item> FRAMED_GLASS = tag("framed_glass");

		private static Tag<Item> tag(String name)
		{
			return new ItemTags.Wrapper(PremiumWoodMod.locate(name));
		}
	}
}
