package com.legacy.premium_wood;

import com.legacy.premium_wood.block.PremiumBlocks;
import com.legacy.premium_wood.item.PremiumItems;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.IForgeRegistryEntry;

@EventBusSubscriber(modid = PremiumWoodMod.MODID, bus = Bus.MOD)
public class PremiumRegistry
{
	@SubscribeEvent
	public static void onRegisterItems(Register<Item> event)
	{
		PremiumItems.init(event);
	}

	@SubscribeEvent
	public static void onRegisterBlocks(Register<Block> event)
	{
		PremiumBlocks.init(event);
	}

	public static <T extends IForgeRegistryEntry<T>> void register(IForgeRegistry<T> registry, String name, T object)
	{
		object.setRegistryName(PremiumWoodMod.locate(name));
		registry.register(object);
	}
}