package com.legacy.premium_wood.world;

import java.util.Random;
import java.util.Set;
import java.util.function.Function;

import com.legacy.premium_wood.block.PremiumBlocks;
import com.legacy.premium_wood.block.natural.AppleLeavesBlock;
import com.mojang.datafixers.Dynamic;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.world.gen.IWorldGenerationReader;
import net.minecraft.world.gen.feature.AbstractTreeFeature;
import net.minecraft.world.gen.feature.TreeFeatureConfig;
import net.minecraftforge.common.IPlantable;

public class AppleTreeFeature extends AbstractTreeFeature<TreeFeatureConfig>
{
	private static final BlockState DEFAULT_TRUNK = Blocks.OAK_LOG.getDefaultState();
	private static final BlockState DEFAULT_LEAF = PremiumBlocks.apple_leaves.getDefaultState();
	protected final int minTreeHeight;
	private final BlockState trunk;
	private final BlockState leaf;

	public AppleTreeFeature(Function<Dynamic<?>, ? extends TreeFeatureConfig> configFactoryIn, boolean doBlockNotifyOnPlace)
	{
		this(configFactoryIn, doBlockNotifyOnPlace, 4, DEFAULT_TRUNK, DEFAULT_LEAF);
	}

	public AppleTreeFeature(Function<Dynamic<?>, ? extends TreeFeatureConfig> configFactoryIn, boolean doBlockNotifyOnPlace, int minTreeHeightIn, BlockState trunkState, BlockState leafState)
	{
		super(configFactoryIn);
		this.minTreeHeight = minTreeHeightIn;
		this.trunk = trunkState;
		this.leaf = leafState;
	}

	// place
	@Override
	protected boolean func_225557_a_(IWorldGenerationReader worldIn, Random rand, BlockPos position, Set<BlockPos> set1, Set<BlockPos> set2, MutableBoundingBox boundsIn, TreeFeatureConfig config)
	{
		int i = this.getHeight(rand);
		boolean flag = true;
		if (position.getY() >= 1 && position.getY() + i + 1 <= worldIn.getMaxHeight())
		{
			for (int j = position.getY(); j <= position.getY() + 1 + i; ++j)
			{
				int k = 1;
				if (j == position.getY())
				{
					k = 0;
				}

				if (j >= position.getY() + 1 + i - 2)
				{
					k = 2;
				}

				BlockPos.Mutable blockpos$mutableblockpos = new BlockPos.Mutable();

				for (int l = position.getX() - k; l <= position.getX() + k && flag; ++l)
				{
					for (int i1 = position.getZ() - k; i1 <= position.getZ() + k && flag; ++i1)
					{
						if (j >= 0 && j < worldIn.getMaxHeight())
						{
							if (!func_214587_a(worldIn, blockpos$mutableblockpos.setPos(l, j, i1)))
							{
								flag = false;
							}
						}
						else
						{
							flag = false;
						}
					}
				}
			}

			if (!flag)
			{
				return false;
			}
			else if (isSoil(worldIn, position.down(), (IPlantable) Blocks.OAK_SAPLING) && position.getY() < worldIn.getMaxHeight() - i - 1)
			{
				this.setDirtAt(worldIn, position.down(), position);

				for (int l2 = position.getY() - 3 + i; l2 <= position.getY() + i; ++l2)
				{
					int l3 = l2 - (position.getY() + i);
					int j4 = 1 - l3 / 2;

					for (int j1 = position.getX() - j4; j1 <= position.getX() + j4; ++j1)
					{
						int k1 = j1 - position.getX();

						for (int l1 = position.getZ() - j4; l1 <= position.getZ() + j4; ++l1)
						{
							int i2 = l1 - position.getZ();
							if (Math.abs(k1) != j4 || Math.abs(i2) != j4 || rand.nextInt(2) != 0 && l3 != 0)
							{
								BlockPos blockpos = new BlockPos(j1, l2, l1);
								if (isAirOrLeaves(worldIn, blockpos) || isTallPlants(worldIn, blockpos))
								{
									if (rand.nextInt(4) == 1)
									{
										this.func_227217_a_(worldIn, blockpos, this.leaf.with(AppleLeavesBlock.MATURE, Boolean.valueOf(true)).with(AppleLeavesBlock.CAN_MATURE, Boolean.valueOf(true)), boundsIn);
									}
									else
									{
										this.func_227217_a_(worldIn, blockpos, this.leaf, boundsIn);
									}
								}
							}
						}
					}
				}

				for (int i3 = 0; i3 < i; ++i3)
				{
					if (isAirOrLeaves(worldIn, position.up(i3)) || isTallPlants(worldIn, position.up(i3)))
					{
						this.func_227217_a_(worldIn, position.up(i3), this.trunk, boundsIn);
						set1.add(position.up(i3).toImmutable());
					}
				}

				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	protected int getHeight(Random random)
	{
		return this.minTreeHeight + random.nextInt(3);
	}
}